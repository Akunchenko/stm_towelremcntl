// Copyright (c) Sandeep Mistry. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

#ifndef LORA_H
#define LORA_H

#include "main.h"
#include "stddef.h"

#define LORA_DEFAULT_SS_PIN    PA4
#define LORA_DEFAULT_RESET_PIN PC13
#define LORA_DEFAULT_DIO0_PIN  PA1

#define PA_OUTPUT_RFO_PIN      0
#define PA_OUTPUT_PA_BOOST_PIN 1



//  LoRaClass();

  int Lora_begin(long frequency);
  void Lora_end(void);

  int Lora_beginPacket(int implicitHeader);
  int Lora_endPacket(void);

  int Lora_parsePacket(int size);
  int Lora_packetRssi(void);
  float Lora_packetSnr(void);

  // from Print
  size_t Lora_write(const uint8_t *buffer, size_t size);

  // from Stream
  int Lora_available(void);
  int Lora_read(void);
  int Lora_peek(void);
  void Lora_flush(void);

  void Lora_onReceive(void(*callback)(int));

  void Lora_receive(int size);
  void Lora_idle(void);
  void Lora_sleep(void);

  void Lora_setTxPower(int level, int outputPin);
  void Lora_setFrequency(long frequency);
  void Lora_setSpreadingFactor(int sf);
  void Lora_setSignalBandwidth(long sbw);
  void Lora_setCodingRate4(int denominator);
  void Lora_setPreambleLength(long length);
  void Lora_setSyncWord(int sw);
  void enableCrc(void);
  void disableCrc(void);

  // deprecated
  void Lora_crc(void);
  void Lora_noCrc(void);

  uint8_t Lora_random(void);

  void Lora_setPins(int ss, int reset, int dio0);
  void Lora_setSPIFrequency(uint32_t frequency);

//  void dumpRegisters(Stream& out);

  void explicitHeaderMode(void);
  void implicitHeaderMode(void);

  void Lora_handleDio0Rise(void);

  uint8_t readRegister(uint8_t address);
  void writeRegister(uint8_t address, uint8_t value);
  uint8_t singleTransfer(uint8_t address, uint8_t value);

  static void Lora_onDio0Rise(void);

  //SPISettings _spiSettings;
  

#endif
