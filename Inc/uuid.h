#ifndef UUID_H
#define UUID_H
#include "stdint.h"
 
#if defined (STM32F205xx) || defined (STM32F407xx)
#define STM32_UUID ((uint32_t *)0x1FFF7A10)
#define STM32_UUID_LEN 12
#else
#define STM32_UUID ((uint32_t *)0x1FFFF7AC)
#define STM32_UUID_LEN 12
#endif

static int32_t GetUuid32(void) {
		return INT32_MAX & (*STM32_UUID + *(STM32_UUID + 1) + *(STM32_UUID + 2));
}

#endif

